# Modelo de Clasificación de Aguas
### Data Scientist:
- Ferrán, Natanael Emir

### Dataset
- [Water Quality](https://www.kaggle.com/datasets/adityakadiwal/water-potability/)
- **Licencia:** *CC0: Public Domain*
- **Autor:** *Aditya Kadiwal*

### A resolver
- Clasificar distintas muestras de agua en potables y no potables

### Técnicas utilizadas
- Logistic Regression
- KNC
- Random forest
- Support Vector Machines